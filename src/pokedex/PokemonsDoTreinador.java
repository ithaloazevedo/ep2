package pokedex;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JList;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class PokemonsDoTreinador {

	 JFrame frame;

	 private Treinador treinadorSelecionado;
	 int index; 
	 Model bancoDeDados;
	/**
	 * Create the application.
	 */
	public PokemonsDoTreinador(int index, Model bancoDeDados) {
		this.treinadorSelecionado = bancoDeDados.treinadores.get(index);
		this.index = index;
		this.bancoDeDados = bancoDeDados;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 346, 784);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 294, 328, 370);
		frame.getContentPane().add(panel);
		panel.setLayout(new GridLayout(1, 1, 0, 0));
		
		JList list = new JList();
		panel.add(list);
		
		JPanel panel_1 = new JPanel();
		panel_1.setBounds(0, 0, 690, 290);
		frame.getContentPane().add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNome.setBounds(12, 13, 78, 34);
		panel_1.add(lblNome);
		
		JLabel lblNometreinador = new JLabel(this.treinadorSelecionado.getNome());
		lblNometreinador.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblNometreinador.setBounds(88, 21, 184, 23);
		panel_1.add(lblNometreinador);
		
		JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblIdade.setBounds(12, 51, 56, 16);
		panel_1.add(lblIdade);
		
		JLabel lblIdadetreinador = new JLabel(Integer.toString(this.treinadorSelecionado.getIdade()));
		lblIdadetreinador.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblIdadetreinador.setBounds(88, 51, 184, 16);
		panel_1.add(lblIdadetreinador);
		
		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblSexo.setBounds(12, 80, 56, 16);
		panel_1.add(lblSexo);
		
		String stringSexo;
		switch(this.treinadorSelecionado.getSexo()) {
		case 'M': {
			stringSexo= "Masculino";
			break;
		}
		case 'F': {
				stringSexo= "Feminino";
				break;
		}
		default: {
			stringSexo= "Outros";
		}
		}
		
		JLabel lblSexotreinador = new JLabel(stringSexo);
		lblSexotreinador.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblSexotreinador.setBounds(88, 80, 115, 16);
		panel_1.add(lblSexotreinador);
		
		JLabel lblId = new JLabel("ID:");
		lblId.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblId.setBounds(12, 109, 56, 16);
		panel_1.add(lblId);
		
		JLabel lblIdtreinador = new JLabel(Integer.toString(this.treinadorSelecionado.getId()));
		lblIdtreinador.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblIdtreinador.setBounds(88, 109, 147, 16);
		panel_1.add(lblIdtreinador);
		
		JButton btnAdicionarPokmon = new JButton("Adicionar pok\u00E9mon");
		btnAdicionarPokmon.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							AddPokemon window = new AddPokemon();
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnAdicionarPokmon.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnAdicionarPokmon.setBounds(73, 215, 177, 23);
		panel_1.add(btnAdicionarPokmon);
		
		JButton btnVerPokmon = new JButton("Ver pok\u00E9mon");
		btnVerPokmon.setFont(new Font("Tahoma", Font.PLAIN, 16));
		btnVerPokmon.setBounds(65, 701, 149, 23);
		frame.getContentPane().add(btnVerPokmon);
	}
}
