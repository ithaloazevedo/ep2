package pokedex;

public class Pokemon {
	private int id; 
	private String name;
	private String type1;
	private String type2;
	private int total;
	private int hp;
	private int attack;
	private int defense;
	private int spAtk;
	private int spDef;
	private int speed;
	private int generation;
	private boolean legendary;
	private int experience;
	private int height;
	private int weight;
	private String abilitie1; 
	private String abilitie2;
	private String abilitie3;
	private String move1; 
	private String move2;
	private String move3;
	private String move4;
	private String move5;
	private String move6;
	private String move7;
	
	public Pokemon() {

	}
	
	public Pokemon(int id, String name, String type1, String type2, int total, int hp, int attack, int defense,
			int spAtk, int spDef, int speed, int generation, boolean legendary, int experience, int height, int weight,
			String abilitie1, String abilitie2, String abilitie3, String move1, String move2, String move3,
			String move4, String move5, String move6, String move7) {
		super();
		this.id = id;
		this.name = name;
		this.type1 = type1;
		this.type2 = type2;
		this.total = total;
		this.hp = hp;
		this.attack = attack;
		this.defense = defense;
		this.spAtk = spAtk;
		this.spDef = spDef;
		this.speed = speed;
		this.generation = generation;
		this.legendary = legendary;
		this.experience = experience;
		this.height = height;
		this.weight = weight;
		this.abilitie1 = abilitie1;
		this.abilitie2 = abilitie2;
		this.abilitie3 = abilitie3;
		this.move1 = move1;
		this.move2 = move2;
		this.move3 = move3;
		this.move4 = move4;
		this.move5 = move5;
		this.move6 = move6;
		this.move7 = move7;
	}
	public int getId() {
		return id;	
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType1() {
		return type1;
	}
	public void setType1(String type1) {
		this.type1 = type1;
	}
	public String getType2() {
		return type2;
	}
	public void setType2(String type2) {
		this.type2 = type2;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public int getHp() {
		return hp;
	}
	public void setHp(int hp) {
		this.hp = hp;
	}
	public int getAttack() {
		return attack;
	}
	public void setAttack(int attack) {
		this.attack = attack;
	}
	public int getDefense() {
		return defense;
	}
	public void setDefense(int defense) {
		this.defense = defense;
	}
	public int getSpAtk() {
		return spAtk;
	}
	public void setSpAtk(int spAtk) {
		this.spAtk = spAtk;
	}
	public int getSpDef() {
		return spDef;
	}
	public void setSpDef(int spDef) {
		this.spDef = spDef;
	}
	public int getSpeed() {
		return speed;
	}
	public void setSpeed(int speed) {
		this.speed = speed;
	}
	public int getGeneration() {
		return generation;
	}
	public void setGeneration(int generation) {
		this.generation = generation;
	}
	public boolean isLegendary() {
		return legendary;
	}
	public void setLegendary(boolean legendary) {
		this.legendary = legendary;
	}
	public int getExperience() {
		return experience;
	}
	public void setExperience(int experience) {
		this.experience = experience;
	}
	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
	}
	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
	}
	public String getAbilitie1() {
		return abilitie1;
	}
	public void setAbilitie1(String abilitie1) {
		this.abilitie1 = abilitie1;
	}
	public String getAbilitie2() {
		return abilitie2;
	}
	public void setAbilitie2(String abilitie2) {
		this.abilitie2 = abilitie2;
	}
	public String getAbilitie3() {
		return abilitie3;
	}
	public void setAbilitie3(String abilitie3) {
		this.abilitie3 = abilitie3;
	}
	public String getMove1() {
		return move1;
	}
	public void setMove1(String move1) {
		this.move1 = move1;
	}
	public String getMove2() {
		return move2;
	}
	public void setMove2(String move2) {
		this.move2 = move2;
	}
	public String getMove3() {
		return move3;
	}
	public void setMove3(String move3) {
		this.move3 = move3;
	}
	public String getMove4() {
		return move4;
	}
	public void setMove4(String move4) {
		this.move4 = move4;
	}
	public String getMove5() {
		return move5;
	}
	public void setMove5(String move5) {
		this.move5 = move5;
	}
	public String getMove6() {
		return move6;
	}
	public void setMove6(String move6) {
		this.move6 = move6;
	}
	public String getMove7() {
		return move7;
	}
	public void setMove7(String move7) {
		this.move7 = move7;
	}
	
	
}
