package pokedex;

import java.util.ArrayList;
import java.util.List;

public class Treinador {
	private String nome;
	private int idade;
	private char sexo;
	private int id;
	private List<Pokemon> pokemons;
	
	public Treinador(){
		this.nome = "";
		this.idade = 0;
		this.sexo = 0;
		this.id = 0;
		setPokemons(new ArrayList<Pokemon>());
	}
	
	Treinador(String nome, int idade, char sexo, int id){
		this.nome = nome;
		this.idade = idade;
		this.sexo = sexo;
		this.id = id;
		setPokemons(new ArrayList<Pokemon>());
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public int getIdade() {
		return idade;
	}
	public void setIdade(int idade) {
		this.idade = idade;
	}
	public char getSexo() {
		return sexo;
	}
	public void setSexo(char sexo) {
		this.sexo = sexo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public List<Pokemon> getPokemons() {
		return pokemons;
	}

	public void setPokemons(List<Pokemon> pokemons) {
		this.pokemons = pokemons;
	}
	
	public void addPokemons(Pokemon pokemon) {
		this.pokemons.add(pokemon);
	}
	
	
}
