package pokedex;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.BorderLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;
import java.awt.Font;

public class Main {
	static private Model bancoDeDados= new Model();
	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
        int id; 
    	String name;
    	String type1;
    	String type2;
    	int total;
    	int hp;
    	int attack;
		 int defense;
		 int spAtk;
		 int spDef;
		 int speed;
		 int generation;
		 boolean legendary;
		 int experience;
		 int height;
		 int weight;
		 String abilitie1; 
		 String abilitie2;
		 String abilitie3;
		 String move1; 
		 String move2;
		 String move3;
		 String move4;
		 String move5;
		 String move6;
		 String move7;
		Path currentRelativePath = Paths.get("");
		String path = currentRelativePath.toAbsolutePath().toString();
		String line1 = "", line2 = "";
        String cvsSplitBy = ",";

        try {
        	BufferedReader br1 = new BufferedReader(new FileReader(path  + "/data/csv_files/POKEMONS_DATA_1.csv"));
        	BufferedReader br2 = new BufferedReader(new FileReader( path  + "/data/csv_files/POKEMONS_DATA_2.csv"));
        	line1 = br1.readLine();	
        	line2 = br2.readLine();
        	while ((line1 = br1.readLine()) != null && (line2 = br2.readLine()) != null ) {
                String[] rawPokemon1 = line1.split(cvsSplitBy);
                String[] rawPokemon2 = line2.split(cvsSplitBy);
                id = Integer.parseInt(rawPokemon1[0]);
                name = rawPokemon1[1];
                type1 = rawPokemon1[2];
                type2 = rawPokemon1[3];
                total = Integer.parseInt(rawPokemon1[4]);
                hp = Integer.parseInt(rawPokemon1[5]);
                attack = Integer.parseInt(rawPokemon1[6]);
                defense = Integer.parseInt(rawPokemon1[7]);
                spAtk = Integer.parseInt(rawPokemon1[8]);
                spDef = Integer.parseInt(rawPokemon1[9]);
                speed = Integer.parseInt(rawPokemon1[10]);
                generation = Integer.parseInt(rawPokemon1[11]);
                legendary = Boolean.parseBoolean(rawPokemon1[12]);
                /*experience = Integer.parseInt(rawPokemon2[2]);
        		height = Integer.parseInt(rawPokemon2[3]);
        		weight = Integer.parseInt(rawPokemon2[4]);
        		abilitie1 = rawPokemon2[5];
        		abilitie2 = rawPokemon2[6];
        		abilitie3 = rawPokemon2[7];
        		move1 = rawPokemon2[8];
        		move2 = rawPokemon2[9];
        		move3 = rawPokemon2[10];
        		move4 = rawPokemon2[11];
        		move5 = rawPokemon2[12];
        		move6 = rawPokemon2[13];
        		move7 = rawPokemon2[14];
        		
                //Pokemon pokemon = new Pokemon(id,name,experience,height,weight,abilitie1,abilitie2,abilitie3,move1,move2,move3,move4,move5,move6,move7);
               // bancoDeDados.pokemons.add(pokemon); */
        	}
        	br1.close();
        	br2.close(); 
        }catch (IOException e) {
            e.printStackTrace();
        }
        
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Main window = new Main();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Main() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	 void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 495, 653);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(new GridLayout(3, 1, 0, 0));
		
		JButton btnPesquisarPokmons = new JButton("Pesquisar pok\u00E9mons");
		btnPesquisarPokmons.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							PesquisarPokemons window = new PesquisarPokemons();
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnPesquisarPokmons.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
		frame.getContentPane().add(btnPesquisarPokmons);
		
		JButton btnVerTreinadores = new JButton("Ver treinadores");
		btnVerTreinadores.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							VerTreinadores window = new VerTreinadores(bancoDeDados);
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnVerTreinadores.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
		frame.getContentPane().add(btnVerTreinadores);
		
		JButton btnCadastrarTreinador = new JButton("Cadastrar treinador");
		btnCadastrarTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							CadastrarTreinador window = new CadastrarTreinador(bancoDeDados);
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			

			}
		});
		btnCadastrarTreinador.setFont(new Font("Trebuchet MS", Font.BOLD, 20));
		frame.getContentPane().add(btnCadastrarTreinador);
	}

	
	
}
