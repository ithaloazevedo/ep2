package pokedex;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JComboBox;

public class AddPokemon {

	JFrame frame;

	/**
	 * Create the application.
	 */
	public AddPokemon() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 357, 187);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblInsiraSeuPokmon = new JLabel("Insira seu pok\u00E9mon:");
		lblInsiraSeuPokmon.setBounds(62, 13, 212, 33);
		lblInsiraSeuPokmon.setFont(new Font("Tahoma", Font.BOLD, 19));
		frame.getContentPane().add(lblInsiraSeuPokmon);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Tahoma", Font.PLAIN, 18));
		comboBox.setBounds(8, 59, 321, 33);
		frame.getContentPane().add(comboBox);
		
	}
}
