package pokedex;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

public class VerTreinadores {

	JFrame frame;
	private Model bancoDeDados;
	
	/**
	 * Create the application.
	 */
	public VerTreinadores(Model bancoDeDados) {
		this.bancoDeDados = bancoDeDados;
		initialize();
	}
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		JButton btnVerTreinador = new JButton("Ver treinador");
		
		JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnVerTreinador.setEnabled(true);
			}
		});

		comboBox.setBounds(77, 84, 274, 41);
		frame.getContentPane().add(comboBox);
		for(Treinador treinador : bancoDeDados.treinadores) {
			comboBox.addItem(treinador.getNome());
		}
		btnVerTreinador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				EventQueue.invokeLater(new Runnable() {
					public void run() {
						try {
							PokemonsDoTreinador window = new PokemonsDoTreinador(comboBox.getSelectedIndex(), bancoDeDados);
							window.frame.setVisible(true);
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
				});
			}
		});
		btnVerTreinador.setBounds(284, 201, 136, 25);
		if(comboBox.getSelectedIndex() == -1) {
			btnVerTreinador.setEnabled(false);
		}
		frame.getContentPane().add(btnVerTreinador);
		
		JLabel lblSelecioneOTreinador = new JLabel("Selecione o treinador:");
		lblSelecioneOTreinador.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		lblSelecioneOTreinador.setBounds(12, 26, 226, 21);
		frame.getContentPane().add(lblSelecioneOTreinador);
	}
}
