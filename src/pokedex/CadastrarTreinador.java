package pokedex;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Font;
import javax.swing.SwingConstants;

import com.sun.javafx.embed.swing.Disposer;

import javax.swing.JSpinner;
import javax.swing.JRadioButton;
import javax.swing.JPanel;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.FlowLayout;
import javax.swing.JTextArea;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Random; 
import java.util.Random;
import javax.swing.SpinnerNumberModel; 

public class CadastrarTreinador {

	JFrame frame;
	private JTextField txtCadastreUmTreinador;
	Model bancoDeDados;
	/**
	 * Create the application.
	 */
	public CadastrarTreinador(Model bancoDeDados) {
		initialize();
		this.bancoDeDados = bancoDeDados;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 403);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		
		txtCadastreUmTreinador = new JTextField();
		txtCadastreUmTreinador.setEditable(false);
		txtCadastreUmTreinador.setHorizontalAlignment(SwingConstants.CENTER);
		txtCadastreUmTreinador.setFont(new Font("Trebuchet MS", Font.BOLD, 16));
		txtCadastreUmTreinador.setText("Cadastre um treinador");
		frame.getContentPane().add(txtCadastreUmTreinador, BorderLayout.NORTH);
		txtCadastreUmTreinador.setColumns(10);
		
		JPanel panel = new JPanel();
		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(new GridLayout(4, 2, 0, 0));
		
		JPanel panel_1 = new JPanel();
		panel.add(panel_1);
		panel_1.setLayout(null);
		
		JLabel lblNome = new JLabel("Nome:");
		lblNome.setBounds(82, 22, 52, 21);
		lblNome.setHorizontalAlignment(SwingConstants.CENTER);
		lblNome.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
		panel_1.add(lblNome);
		
		JPanel panel_2 = new JPanel();
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		JTextArea nome = new JTextArea();
		nome.setBounds(12, 19, 192, 27);
		panel_2.add(nome);
		
		JPanel panel_3 = new JPanel();
		panel.add(panel_3);
		panel_3.setLayout(null);
		
		JLabel lblIdade = new JLabel("Idade:");
		lblIdade.setBounds(82, 22, 51, 21);
		lblIdade.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
		panel_3.add(lblIdade);
		
		JPanel panel_4 = new JPanel();
		panel.add(panel_4);
		panel_4.setLayout(null);
		
		JSpinner idade = new JSpinner();
		idade.setFont(new Font("Tahoma", Font.PLAIN, 16));
		idade.setModel(new SpinnerNumberModel(0, 0, 99, 1));
		idade.setBounds(86, 13, 43, 40);
		panel_4.add(idade);
		
		JPanel panel_5 = new JPanel();
		panel.add(panel_5);
		panel_5.setLayout(null);
		
		JLabel lblSexo = new JLabel("Sexo:");
		lblSexo.setBounds(85, 22, 45, 21);
		lblSexo.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
		panel_5.add(lblSexo);
		
		JPanel panel_6 = new JPanel();
		panel.add(panel_6);
		panel_6.setLayout(null);
		
		@SuppressWarnings("rawtypes")
		JComboBox sexo = new JComboBox();
		sexo.setFont(new Font("Trebuchet MS", Font.PLAIN, 18));
		sexo.setModel(new DefaultComboBoxModel(new String[] {"Masculino", "Feminino", "Outros"}));
		sexo.setBounds(52, 19, 112, 27);
		panel_6.add(sexo);
		JPanel panel_7 = new JPanel();
		panel.add(panel_7);
		
		JPanel panel_8 = new JPanel();
		panel.add(panel_8);
		panel_8.setLayout(null);
		
		JButton btnSalvar = new JButton("Salvar");
		btnSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				char charSexo;
				switch(sexo.getSelectedIndex()) {
				case 0: {
					charSexo= 'M';
					break;
				}
				case 1: {
						charSexo= 'F';
						break;
				}
				default: {
					charSexo= 'O';
				}
				}
				int pickedNumber;
				do {
					Random rand = new Random();
					pickedNumber = rand.nextInt(100) + 1;
				} while(checkNumber(pickedNumber, bancoDeDados) == false);
				
				Treinador treinador = new Treinador(nome.getText(), (int)idade.getValue(), charSexo, pickedNumber);
				bancoDeDados.treinadores.add(treinador);
				JOptionPane.showMessageDialog(null, "Seu id �: " + pickedNumber + ".", "Cadastro realizado com sucesso!", JOptionPane.INFORMATION_MESSAGE);
				frame.dispose();
			}
		
		});
		btnSalvar.setBounds(103, 33, 101, 36);
		btnSalvar.setFont(new Font("Trebuchet MS", Font.BOLD, 18));
		panel_8.add(btnSalvar);
	}
	private boolean checkNumber(int pickedNumber, Model model) {
			for (Treinador treinador : model.treinadores) {
				if (treinador.getId() == pickedNumber) return false;
			}
			return true;
	}
}
